import qbs

CppApplication {
    consoleApplication: true
    files: [
        "mycat.c",
    ]

    Group {     // Properties for the produced executable
        fileTagsFilter: "application"
        qbs.install: true
        qbs.installDir: "bin"
    }
}
