#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

bool d1 = false;
bool d2 = false;

int getl(char s[], int mxl)
{
    int ind = 0;
    int c;
    while (ind < mxl-1 && (c = getchar()) != EOF)
    {
        *(s+ind) = c;
        ind++;
        if (c == '\n')
        {
            break;
        }
    }

    *(s+ind) = '\0';
    int len = strlen(s);
    if (d2) printf("String is '%s'\nlen = %i\n", s, len);

    return len;
}

int main()
{
    int bsize = 16;
    char *buff = malloc(bsize * sizeof(char));
    int l;
    while ((l = getl(buff, bsize)) == bsize-1)
    {
        printf("%s", buff);
        if (d1) printf("(%i)", l);
    }
    printf("%s", buff);
    if (d1) printf("(%i)", l);
    free(buff);
    return 0;
}
